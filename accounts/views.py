from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from .forms import SignUpForm

def index_acc(request):
	form = SignUpForm()
	return render(request, 'acc.html', {'form' : form})

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()  
            user.profile.role = form.cleaned_data.get('role')
            user.save()
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            login(request, user)
            return redirect('/')
    else:
        form = SignUpForm()
    return render(request, 'acc.html', {'form': form})

def index_login(request):
    form = SignUpForm()
    return render(request, 'acc_login.html')

def auth_login(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username , password=password)
        if user is not None:
            login(request, user)
            return redirect('/')
    return redirect('/accounts/login')

@login_required
def logoutView(request):
    if request.method == "GET":
        if request.user.is_authenticated:
            logout(request)
    return redirect('/')