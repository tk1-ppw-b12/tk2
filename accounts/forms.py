from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class SignUpForm(UserCreationForm):
	ROLES = [('Client', 'Client'), ('Contractor', 'Contractor')]
	first_name = forms.CharField(max_length=50)
	last_name = forms.CharField(max_length=50)
	email = forms.EmailField(max_length=50)
	role = forms.CharField(widget=forms.Select(choices=ROLES))

	class Meta:
		model = User
		fields = ('username', 'first_name', 'last_name', 'email', 'role', 'password1', 'password2', )