from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import *
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class AccountsUnitTest(TestCase):
	def test_url_is_exist(self):
		response = Client().get('/accounts/')
		self.assertEqual(response.status_code,200)

	def test_url_is_not_exist(self):
		response = Client().get('/invalid/')
		self.assertEqual(response.status_code,404)

	def test_using_accounts_template(self):
		response = Client().get('/accounts/')
		self.assertTemplateUsed(response, 'acc.html')

	def test_using_view_index_func(self):
		found = resolve('/accounts/')
		self.assertEqual(found.func, index_acc)

	def test_login_url_is_exist(self):
		response = Client().get('/accounts/login')
		self.assertEqual(response.status_code,301)

	def test_signup_url_is_exist(self):
		response = Client().get('/accounts/signup')
		self.assertEqual(response.status_code,301)

	def test_forms_validation(self):
		form_data = {'username' : 'hai', 'password1': 'haihaihai', 'password2': 'haihaihai'}
		form = UserCreationForm(data=form_data)
		self.assertTrue(form.is_valid())

	def test_POST_success_to_save_status(self):
		counting_status_object = User.objects.count()
		response = Client().post('/accounts/signup', data={'username' : 'hai', 'password1': 'haihaihai', 'password2': 'haihaihai'})
		self.assertEqual(counting_status_object, 0)
		self.assertEqual(response.status_code, 301)

	def test_model_create_new_status(self):
		new_status = User.objects.create(username= 'hai', password = 'haihaihai')
		counting_status_object = User.objects.all().count()
		self.assertEqual(counting_status_object, 1)

	def test_login_success(self):
		user = User.objects.create(username='testuser')
		user.set_password('12345')
		user.save()
		c = Client()
		logged_in = c.login(username='testuser', password='12345')

	def test_logout_url_is_exist(self):
		response = Client().get('/accounts/logout')
		self.assertEqual(response.status_code, 301)