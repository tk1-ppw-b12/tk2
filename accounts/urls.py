from .views import *
from django.shortcuts import render
from django.urls import path

app_name = 'accounts'
urlpatterns = [
    path('', index_acc, name='index'),
    path('signup/', signup, name='signup'),
    path('login/', index_login, name='in'),
    path('authlogin/', auth_login, name='login'),
    path('logout/', logoutView, name='logout')
]
