function initMap(){
    var options = {
        zoom:8,
        center:{lat:-6.385589,lng:106.830711}
    }

    var map = new google.maps.Map(document.getElementById('map'),options);

    var marker = new google.maps.Marker({
        position:{lat:-6.385589,lng:106.830711},
        map:map
    });
}

$(document).ready(function () {
    $('input[type=checkbox]').change(function(){
        if ($(this).is(':checked')) {
            $('.rev-container').css("background-color","#173F5F");
            $('body').css("background-color","#173F5F");
            $('p').css("color", "white");
            $('label').css("color", "white");
            $('small').css("color", "white");

        }else{
            $('.rev-container').css("background-color","#60BBB6");
            $('body').css("background-color","#60BBB6");
            $('p').css("color","black");
            $('label').css("color", "black");
            $('small').css("color", "black");
        }
    });
});

$('#user_form').submit(function(e) {
    e.preventDefault();
    $form = $(this)
    var formData = new FormData(this);
    $.ajax({
        method:'POST',
        dataType:'json',
        url:'save_project/',
        data: formData,
        success:function(data){
            console.log(data);  
            alert("Form has been saved!");
        },
        error:function(){
            alert("error");
        },
        cache:false,
    });
});