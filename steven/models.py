# Create your models here.
from django.db import models
import datetime

class Contractor(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=50)
    email = models.CharField(max_length=50)
    project = models.CharField(max_length=50)
    link = models.CharField(max_length=50)
    detail = models.TextField()
