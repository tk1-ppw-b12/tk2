from .views import *
from django.shortcuts import render
from django.urls import path

app_name = 'steven'
urlpatterns = [
    path('', project, name='project'),
    path('save_project/', save_project, name='save_project'),
    path('rq_delete/<id>', rq_delete, name='rq_delete'),
]
