from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from .models import Question

class HomeTest(TestCase):
	def test_home_url_is_exist(self):
		response = Client().get('')
		self.assertEqual(response.status_code,200)

	def test_home_using_home_template(self):
		response = Client().get('')
		self.assertTemplateUsed(response, 'home.html')

	def test_home_using_view_rev_func(self):
		found = resolve('/')
		self.assertEqual(found.func, welcome)

	def test_model_create_new_question(self):
		new_question = Question.objects.create(name='Nabila Azzahra', email='ilmarohma', message='saya penasaran hmm')
		counting_question_object = Question.objects.all().count()
		self.assertEqual(counting_question_object, 1)

	
     
