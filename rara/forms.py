from django import forms
from .models import Question
from django.forms import ModelForm

class QuestionForm(ModelForm):
    class Meta:
        model= Question
        fields = ['name', 'email','message']
        labels = {
            'name': 'Name','email':'Email', 'message':'message',
        }
        widgets = {
            'name' : forms.TextInput(attrs={'class': 'form-control',
                                        'type' : 'text',
                                        'placeholder' : 'What\'s your name?'}),
            'email' : forms.TextInput(attrs={'class': 'form-control',
                                        'type' : 'email',
                                        'placeholder' : 'What\'s your email?'}),
            'message' : forms.Textarea(attrs={'class': 'form-control',
                                        'type' : 'textarea', 'rows' : '3',  
                                        'placeholder' : 'What\'s your message?'}),
        }