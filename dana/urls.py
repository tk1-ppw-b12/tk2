from . import views
from .views import *
from django.shortcuts import render
from django.urls import path

app_name = 'dana'
urlpatterns = [
    path('', req, name='req'),
    path('add_request/', add_request, name='add_request'),
    path('rq_delete/<id>', views.rq_delete, name='rq_delete'),
    path('ganti_nama/', views.ganti_nama, name='ganti_nama')
]
