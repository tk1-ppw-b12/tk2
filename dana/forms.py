from django import forms
from .models import Request
from django.forms import ModelForm

class RequestForm(ModelForm):
    class Meta:
        model= Request
        fields = ['name', 'email', 'wideScale', 'budget','detail']
        labels = {'name': 'Name','email':'Email', 'wideScale':'Wide-Scale', 'budget':'Budget', 'detail':'Detail'}
        widgets = {
            'name' : forms.TextInput(attrs={'class': 'form-control',
                                        'type' : 'text',
                                        'placeholder' : 'What\'s your name?'}),
            'email' : forms.TextInput(attrs={'class': 'form-control',
                                        'type' : 'email',
                                        'placeholder' : 'What\'s your email?'}),
            'wideScale' : forms.TextInput(attrs={'class': 'form-control',
                                        'type' : 'text',
                                        'placeholder' : 'How big is the Area? (ex : 100 m2)'}),                           
            'budget' : forms.Select(attrs={'class': 'form-control'}),
            'detail' : forms.Textarea(attrs={'class': 'form-control',
                                        'type' : 'textarea', 'rows' : '3'}),
        }
