from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from .models import Request
from django.utils import timezone


class RequestTest(TestCase):
	def test_request_url_is_exist(self):
		response = Client().get('/request/')
		self.assertEqual(response.status_code,200)

	def test_request_using_request_template(self):
		response = Client().get('/request/')
		self.assertTemplateUsed(response, 'request.html')

	def test_request_using_view_rev_func(self):
		found = resolve('/request/')
		self.assertEqual(found.func, req)

	def test_model_create_new_request(self):
		new_request = Request.objects.create(date=timezone.now(), name='Fauzan Pradana Linggih', email='fpradana@gmail.com', wideScale = '100 m2', budget='100-300 M', detail='Yang bagus lah pokoknya')
		counting_request_object = Request.objects.all().count()
		self.assertEqual(counting_request_object, 1)

	def test_model_create_new_review(self):
		new_review = Contractor.objects.create(date=timezone.now(), name='Dana', email='danaemail@gmail.com', project='your', link='google.com', detail='woy')
		counting_review_object = Contractor.objects.all().count()
		self.assertEqual(counting_review_object, 1)

	def test_forms_valid(self):
		form_data = {'name': 'Dana','email':'dana@gmail.com', 'wideScale':'500 M2', 'budget':'100-300 M', 'detail':'skituketo'}
		form = RequestForm(data=form_data)
		self.assertTrue(form.is_valid())

	def test_forms_not_valid(self):
		form_data = {'name': 'Dana'*500,'email':'danamail.com', 'wideScale':'500 M2'*500, 'budget':'100-300 M'*500, 'detail':'skituketo'*500}
		form = RequestForm(data = form_data)
		self.assertFalse(form.is_valid())

     
