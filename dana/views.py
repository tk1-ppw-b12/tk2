from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from .forms import RequestForm
from .models import Request
from steven.models import Contractor
import datetime
import json

def req(request):
	data = Contractor.objects.all()
	form = RequestForm(request.POST)
	content = {'form' : form, 'data' : data}
	return render(request, 'request.html', content)

def add_request(request):
	if request.method == 'POST':
		form = RequestForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('/request/')
	else:
		return redirect('/request/')


def rq_delete(request, id):
    rq = Contractor.objects.filter(id=id)
    rq.delete()
    return redirect('dana:req')
    
def ganti_nama(request):
    response = {}
    nama = request.POST["nama"]
    response['nama'] = nama
    return JsonResponse(response)
        