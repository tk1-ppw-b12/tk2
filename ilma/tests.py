from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from .models import Review
from django.utils import timezone


class ReviewTest(TestCase):
	#general tests
	def test_review_url_is_exist(self):
		response = Client().get('/review/')
		self.assertEqual(response.status_code,200)

	def test_review_using_review_template(self):
		response = Client().get('/review/')
		self.assertTemplateUsed(response, 'review.html')

	#function test
	def test_review_using_view_rev_func(self):
		found = resolve('/review/')
		self.assertEqual(found.func, rev)

	#model tests
	def test_model_create_new_review(self):
		new_review = Review.objects.create(name='Ilma Ainur Rohma', email='ilma13rohma@gmail.com', rate='5', comment='keren banget euy')
		counting_review_object = Review.objects.all().count()
		self.assertEqual(counting_review_object, 1)

	def test_data_displayed(self):
	 	response = Client().post('/review/create_rev/', data={'name' : 'Ilma Ainur Rohma', 'email' : 'ilma13rohma@gmail.com', 'rate': '5', 'comment' : 'keren banget cuy'})

	 	self.assertEqual(response.status_code, 200)

	 	new_response = self.client.get('/review/')
	 	html_response = new_response.content.decode()
	 	self.assertIn('Ilma Ainur Rohma', html_response)
	 	self.assertIn('5', html_response)
	 	self.assertIn('keren banget cuy', html_response)

	#forms tests
	def test_forms_valid(self):
		form_data = {'name' : 'Ilma Ainur Rohma', 'email' : 'ilma13rohma@gmail.com', 'rate': '5', 'comment' : 'keren banget cuy'}
		form = ReviewForm(data=form_data)
		self.assertTrue(form.is_valid())

	def test_forms_not_valid(self):
		form_data = {'name' : 'Ilma Ainur Rohma'*500, 'email' : 'ilma13rohmagmail.com', 'rate': '9'*500, 'comment' : 'keren banget cuy'*500}
		form = ReviewForm(data = form_data)
		self.assertFalse(form.is_valid())
