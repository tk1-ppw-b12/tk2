from django.db import models
import datetime

# Create your models here.

RATE=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5')]

class Review(models.Model):
	#date = models.DateTimeField(auto_now_add=True)
	name = models.CharField(max_length=50)
	email = models.CharField(max_length=50)
	rate = models.CharField(max_length=50, choices=RATE)
	comment = models.TextField()
