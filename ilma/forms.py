from django import forms
from .models import Review
from django.forms import ModelForm

class ReviewForm(ModelForm):
    class Meta:
        model= Review
        fields = ['name', 'email', 'rate', 'comment']
        labels = {
            'name': 'Name','email':'Email', 'rate':'Rate', 'comment':'Comment',
        }
        widgets = {
            'name' : forms.TextInput(attrs={'class': 'form-control',
                                        'type' : 'text',
                                        'placeholder' : 'What\'s your name?'}),
            'email' : forms.TextInput(attrs={'class': 'form-control',
                                        'type' : 'email',
                                        'placeholder' : 'What\'s your email?'}),
            'rate' : forms.Select(attrs={'class': 'form-control'}),
            'comment' : forms.Textarea(attrs={'class': 'form-control',
                                        'type' : 'textarea', 'rows' : '3'}),
        }
